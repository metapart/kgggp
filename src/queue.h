#ifndef __QUEUE_H__
#define __QUEUE_H__

//@{

/* *********************************************************** */
/*                          QUEUE                              */
/* *********************************************************** */

/** Queue structure. */
typedef struct Queue_ {
  int *tab;
  int size;
  int start;
  int end;
} Queue;

void queue_init (Queue *self, int size);
void queue_push_back (Queue* self, int v);
int queue_pop_front (Queue* self);
int queue_is_empty (const Queue *self);
void queue_free (Queue *self);

//@}

#endif
