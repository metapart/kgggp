/* K-way Greedy Graph Growing Partitioning */

#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "kgggp.h"
#include "struct.h"
#include "seeds.h"

/* #define DEBUG */
/* #define DEBUGADV */
/* #define DEBUG_PERIOD 100 */
/* #define WARNING */
#include "debug.h"

/* *********************************************************** */

#define EPSILON 1e-6
#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))

/* *********************************************************** */
/*                  KGGGP BUCKET STRUCT                        */
/* *********************************************************** */

typedef enum BucketIndexType_ {
  KGGGP_BUCKET_NO = -1,    // removed from buckets
  KGGGP_BUCKET_REG = 0,    // regular gain bucket (REG)
  KGGGP_BUCKET_NCC,        // no-connectivity-constraint bucket  (NCC)
  KGGGP_BUCKET_NBC,        // no-balance-constaint gain bucket  (NBC)  
  KGGGP_BUCKET_NBR         // number of buckets        
} BucketIndexType;

const char * bucketName[KGGGP_BUCKET_NBR] = {"REG", "NCC", "NBC" };

/* *********************************************************** */

struct candidate { 
  int vtx;
  int gain;
  int part;
  int conn;                /* nb of vtx neighbors in same part */
  BucketIndexType buckidx; /* bucket index */ 
  struct candidate * next;
  struct candidate * prev;
};

/* *********************************************************** */

struct kgggp_t {
  int nparts;
  int nvtxs;
  int maxgain;
  int bucketsize;                                 /* bucketsize = 2*maxgain+1 with possible gains: [-maxgain, +maxgain] */
  int bestgain[KGGGP_BUCKET_NBR];                 /* current best gain that provide candidate(s) */
  struct candidate** buckets[KGGGP_BUCKET_NBR];   /* array of buckets, such as buckets[g] is a chained-list of all candidates with the gain (g-maxgain) */
  int nbcandidates[KGGGP_BUCKET_NBR];             /* current nb of candidates in each buckets */
  struct candidate*** list;                       /* list[p][v] is the candidate pointer to vertex v moving to part p */
  struct candidate* allcandidates;                /* contiguous array of nparts*nvtxs candidates */  
};


/* *********************************************************** */
/*                     KGGGP ROUTINES                          */
/* *********************************************************** */

static void kgggp_bucket_remove(struct kgggp_t * s, struct candidate * c)
{
  assert(c);
  // printf("remove vtx %d from bucket %d\n", c->vtx, c->buckidx);
  assert(c->buckidx >= 0 && c->buckidx < KGGGP_BUCKET_NBR);
  
  if(!c->prev) {
    assert(s->buckets[c->buckidx][s->maxgain-c->gain] == c);    
    s->buckets[c->buckidx][s->maxgain-c->gain] = c->next;
  }
  if(c->prev) {
    assert(c->prev->next == c);
    assert(c->buckidx == c->prev->buckidx);
    c->prev->next = c->next;
  }
  if(c->next) {
    assert(c->next->prev == c);
    assert(c->buckidx == c->next->buckidx);
    c->next->prev = c->prev;
  }
  
  // #pragma omp atomic update  // SHOULD BE MOVED TO MTKGGGP
  // #pragma omp atomic
  s->nbcandidates[c->buckidx]--;
  
  c->prev = c->next = NULL;
  c->buckidx = KGGGP_BUCKET_NO;
}

/* *********************************************************** */

static void kgggp_bucket_insert(struct kgggp_t * s, struct candidate * c)
{
  assert(c);
  assert(c->buckidx >= 0 && c->buckidx < KGGGP_BUCKET_NBR);  
  
  struct candidate * next = s->buckets[c->buckidx][s->maxgain-c->gain];
  if(next) {
    assert(c->buckidx == next->buckidx);
    next->prev = c;
  }
  c->next = next;
  c->prev = NULL;
  s->buckets[c->buckidx][s->maxgain-c->gain] = c;
  if(c->next) assert(c->next->prev == c);
  
  // #pragma omp atomic update  // SHOULD BE MOVED TO MTKGGGP
  // #pragma omp atomic
  s->nbcandidates[c->buckidx]++;
  
  if(c->gain > s->bestgain[c->buckidx]) s->bestgain[c->buckidx] = c->gain; // update best gain
}

/* *********************************************************** */

static void kgggp_reset(struct kgggp_t * s, int nparts, int nvtxs)
{
  for(int i = 0 ; i < KGGGP_BUCKET_NBR ; i++) {
    s->nbcandidates[i] = 0;    
    for(int v = 0 ; v < s->bucketsize ; v++) s->buckets[i][v] = NULL;
  }
  
  for(int k = 0 ; k < nparts*nvtxs ; k++) {
    s->allcandidates[k].vtx = -1;
    s->allcandidates[k].part = -1;
    s->allcandidates[k].gain = -1; 
    s->allcandidates[k].conn = -1;
    s->allcandidates[k].buckidx = KGGGP_BUCKET_NO;
    s->allcandidates[k].next = NULL;
    s->allcandidates[k].prev = NULL;    
  }
  
}

/* *********************************************************** */

static void kgggp_alloc(struct kgggp_t * s, int maxgain, int nparts, int nvtxs)
{
  int bucketsize = 2*maxgain+1; /* possible gains: [-maxgain, +maxgain] */  
  
  for(int i = 0 ; i < KGGGP_BUCKET_NBR ; i++) {
    s->buckets[i] = malloc(sizeof(struct candidate*)*bucketsize);
    s->bestgain[i] = -maxgain;
  }
  
  struct candidate* allcandidates = malloc(nparts*nvtxs*sizeof(struct candidate));
  assert(allcandidates);
  
  struct candidate*** list;
  list = malloc(nparts*sizeof(struct candidate**));
  for(int k = 0 ; k < nparts ; k++) {
    list[k] = malloc(sizeof(struct candidate*)*nvtxs);
    for(int i = 0 ; i < nvtxs ; i++) list[k][i] = allcandidates + (k*nvtxs+i);  
  }  
  
  /* init kgggp struct */
  s->maxgain = maxgain;
  s->bucketsize = bucketsize;
  s->nparts = nparts;
  s->nvtxs = nvtxs;
  s->list = list;
  s->allcandidates = allcandidates;
  
  // reset candidates
  kgggp_reset(s, nparts, nvtxs);
  
}

/* *********************************************************** */

static void kgggp_free(struct kgggp_t * s)
{
  for(int i = 0 ; i < KGGGP_BUCKET_NBR ; i++) free(s->buckets[i]);
  for(int k = 0 ; k < s->nparts ; k++) free(s->list[k]);  
  free(s->list);
  free(s->allcandidates);
}

/* *********************************************************** */

static double kgggp_compute_maxub(Graph * g, 
				  int nparts, 
				  int * part) 
{    
  double wparts[nparts];
  double wtot = 0.0;
  for(int k = 0 ; k < nparts ; k++) wparts[k] = 0.0;
  for(int i = 0; i < g->nvtxs ; i++) {
    if(part[i] >= 0  && part[i] < nparts) {
      int w = (g->vwgts != NULL)?g->vwgts[i]:1;
      wparts[part[i]] += w;
      wtot += w;
    }
  }  
  
  double maxub = 0.0;
  for (int k = 0; k < nparts; k++)
    if (wparts[k] > maxub) maxub = wparts[k];
  maxub /= (wtot/nparts);
  
  return maxub-1.0;
}

/* *********************************************************** */

static int kgggp_compute_edgecuts(Graph * g,       			 
				  int nparts,      
				  int * part,      
				  int * edgecuts   /* array of size nparts (or NULL) */
				  )
{
  int edgecut = 0;
  int _edgecuts[nparts];
  
  for(int k = 0 ; k < nparts ; k++) _edgecuts[k] = 0;
  
  for(int i = 0 ; i < g->nvtxs ; i++) {
    int k = part[i];
    if(k == -1) continue; /* skip if free */
    for(int j = g->xadj[i] ; j < g->xadj[i+1] ; j++) {
      int ii =  g->adjncy[j]; /* edge (i,ii) */
      int kk = part[ii];
      if(kk != k) _edgecuts[k] += (g->ewgts ? g->ewgts[j] : 1); /* part -1 is external */
    }
  }
  
  for(int k = 0 ; k < nparts ; k++) {
    edgecut += _edgecuts[k];
    if(edgecuts) edgecuts[k] = _edgecuts[k];
  }
  
  return (edgecut/2);
}

/* *********************************************************** */

static int kgggp_compute_maxgain(Graph * g, int * part)
{
  int maxgain = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) {
    if(part && part[i] != -1) continue; /* skip vertices if not free */
    int gain = 0;
    if (!g->ewgts)
      gain = g->xadj[i+1] - g->xadj[i]; // gain = degree
    else
      for (int j = g->xadj[i]; j < g->xadj[i+1]; j++)
	gain += g->ewgts[j];
    if(gain > maxgain) maxgain = gain;
  }
  return maxgain;
}

/* *********************************************************** */

/* compute gain relative to edge j, if vertex i move to part k */
static int kgggp_gain_init(Graph *g, int * part, int i, int k, int j, int gainscheme, int internaledgefactor)
{
  // check
  assert(part[i] == -1 || part[i] == k);
  
  int gain = 0;
  
  // GAIN DEFINITION
  // Assuming vertex i is moving in part k, lets define:
  // Nint(i) : the nb of internal edges (adjacent edges to vtx i with endpoints in same part k)
  // Nfree(i) : the nb of free edges (adjacent edges to vtx i with endpoints in part -1)
  // Next(i) : the nb of external edges (adjacent edges to vtx i with endpoints not in same part k, not in free part)
  
  int ii =  g->adjncy[j]; /* edge (i,ii) */			  
  int ew = (g->ewgts ? g->ewgts[j] : 1);
  
  if (gainscheme == KGGGP_GAIN_DIFF) {
    if(part[ii] == k) gain += internaledgefactor*ew; else if(part[ii] != -1) gain -= ew; // DIFF (gain = Nint(i) - Next(i))
  }
  else if (gainscheme == KGGGP_GAIN_CLASSIC) {
    if(part[ii] == k) gain += internaledgefactor*ew; else if(part[ii] == -1) gain -= ew; // CLASSIC FM (gain = Nint(i) - Nfree(i))
  }
  else if (gainscheme == KGGGP_GAIN_HYBRID) {
    if(part[ii] == k) gain += internaledgefactor*ew; else gain -= ew; // HYBRID (gain = Nint(i) - Next(i) - Nfree(i))
  }
  else {
    fprintf(stderr, "ERROR: KGGGP GAIN undefined!!!\n");
    abort();
  }
  
  return gain;
}

/* *********************************************************** */

/* graph g, edge j = (i,ii)  */
/* vertex i is moving to part k, so update neighborhood gains of free vertices... */
/* more precisely, update gain if vertex ii (connected to i by edge j) will move to part kk */

static int kgggp_gain_update(Graph *g, int * part, int i, int k, int j, int kk, int gainscheme, int internaledgefactor)
{
  int updategain = 0;
  int ew = (g->ewgts ? g->ewgts[j] : 1);
  int ii =  g->adjncy[j]; /* edge (i,ii) */
  assert(part[ii] == -1); /* assert already skip, no update required */  
  
  // Assuming vertex i move from -1 to k, I should update gain for the neighbor candidate (ii,kk)
  // 1) if k == kk, I win one internal vertex (+factor*ew for DIFF/CLASSIC/HYBRID) and I lost one free vertex (+ew for CLASSIC/HYBRID)
  // 2) if k != kk, I win one external vertex (-ew for DIFF/HYBRID) and I lost one free vertex (+ew for CLASSIC/HYBRID)
  
  if (gainscheme == KGGGP_GAIN_DIFF) {
    if(k == kk) updategain =internaledgefactor*ew; else updategain = -ew; 
  }
  else if (gainscheme == KGGGP_GAIN_CLASSIC) {
    if(k == kk) updategain = (internaledgefactor+1)*ew; else updategain = ew; 
  }
  else if (gainscheme == KGGGP_GAIN_HYBRID) {
    if(k == kk) updategain = (internaledgefactor+1)*ew; else updategain = 0; 
  }
  else {
    fprintf(stderr, "ERROR: KGGGP GAIN undefined!!!\n");
    abort();
  }
  
  return updategain;
}

/* *********************************************************** */

/* compute gain if vertex i move to part k */
static void kgggp_candidate_init(Graph *g, int * part, struct candidate * c, int i, int k, int gainscheme, int internaledgefactor)
{
  // check
  assert(c);
  assert(part[i] == -1 || part[i] == k);
  
  /* compute gain if vertex i move to k */ 
  int gain = 0;
  int conn = 0;
  for(int j = g->xadj[i] ; j < g->xadj[i+1] ; j++) { /* for all edges */		  
    int ii =  g->adjncy[j]; /* edge (i,ii) */
    if(part[ii] == k) conn++;
    gain += kgggp_gain_init(g, part, i, k, j, gainscheme, internaledgefactor);    
  }
  
  c->vtx = i;
  c->conn = conn; 
  c->part = k;
  c->buckidx = KGGGP_BUCKET_REG;  
  c->gain = gain;  
}

/* *********************************************************** */

static void kgggp_init(struct kgggp_t * s, Graph * g, int * part, int gainscheme, int internaledgefactor, int greedy) 
{  
  int nbcandidates = 0;
  
  /* GREEDY GLOBAL */
  if(greedy == KGGGP_GREEDY_GLOBAL) {
    
    int * perm = NULL;
    
#ifdef RANDOM_INIT
    perm = malloc(sizeof(*perm) * g->nvtxs);
    randomPermute(g->nvtxs, perm);
#endif
    
    /* for all vertices */
    for(int ii = 0 ; ii < g->nvtxs ; ii++) {
      int i;
      if (perm) i = perm[ii]; else i = ii;
      if(part[i] != -1) continue; /* skip if not free */
      
      /* for all target part k */
      for(int k = 0 ; k < s->nparts ; k++) {      
	
	/* init & insert candidate */
	struct candidate * current = s->list[k][i]; /* already allocated in kgggp_alloc() */
	assert(current);
	assert(current->buckidx == KGGGP_BUCKET_NO);	
	// assert(current->next == NULL && current->prev == NULL);
	kgggp_candidate_init(g, part, current, i, k, gainscheme, internaledgefactor);
	assert(current->gain >= -s->maxgain && current->gain <= s->maxgain);
	kgggp_bucket_insert(s, current);	
	nbcandidates++;
      }   
      
    }
    
  }
  
  /* GREEDY LOCAL */
  else { 
    
    /* for all vertices */
    for(int i = 0 ; i < g->nvtxs ; i++) {    
      
      if(part[i] == -1) continue; /* skip if free */
      
      /* just consider the neighborhood of initial vertices */
      int k = part[i];      
      
      /* init & insert candidate */
      for(int j = g->xadj[i] ; j < g->xadj[i+1] ; j++) { /* for all edges */		  
	int ii =  g->adjncy[j]; /* edge (i,ii) */
	if(part[ii] != -1) continue; // skip, vertex ii already assigned
	struct candidate * current = s->list[k][ii];
	assert(current);
	if(current->buckidx != KGGGP_BUCKET_NO && current->vtx == ii && current->part == k) continue; // skip, already added	
	assert(current->buckidx == KGGGP_BUCKET_NO);
	assert(part[ii] == -1);
	// assert(current->next == NULL && current->prev == NULL);	
	kgggp_candidate_init(g, part, current, ii, k, gainscheme, internaledgefactor);
	assert(current->gain >= -s->maxgain && current->gain <= s->maxgain);
	kgggp_bucket_insert(s, current);
	nbcandidates++;
      }      
    }  
    
  }
  
  PRINTADV("nb candidates: %d\n", nbcandidates);
  assert(s->nbcandidates[KGGGP_BUCKET_REG] == nbcandidates);
}

/* *********************************************************** */

static void kgggp_check(struct kgggp_t * s, Graph * g, int * part)
{  
  /* check candidates... */
  for(int k = 0 ; k < s->nparts ; k++) {  
    for(int i = 0 ; i < g->nvtxs ; i++) {
      struct candidate * c = s->list[k][i];
      assert(c);
      // check candidate
      if(c->buckidx != KGGGP_BUCKET_NO) {
	// if(part[i] == -1) assert(c->vtx == i);	
	assert(c->vtx == i);	
	assert(c->part >= -1 && c->part < s->nparts);
	assert(c->buckidx >= 0 && c->buckidx < KGGGP_BUCKET_NBR);  	
      }
    }	
  }
  
  for(int buckidx = 0 ; buckidx < KGGGP_BUCKET_NBR ; buckidx++) {    
    int nbcandidates = 0;    
    for(int v = 0 ; v < s->bucketsize ; v++) {
      // PRINTADV("buckets[%d][%d]: {", k, maxgain - v);
      struct candidate * c = s->buckets[buckidx][v];      
      while(c != NULL) {
	// PRINTADV("%d ",c->vtx);	
	assert(c->buckidx == buckidx);
	assert(c->gain == s->maxgain - v);
	assert(c->gain <= s->bestgain[buckidx]);
	assert(c->gain >= -s->maxgain && c->gain <= +s->maxgain);	
	assert(c->vtx >= 0 && c->vtx < g->nvtxs);
	assert(part[c->vtx] == -1);
	if(c->next) assert(c->next->prev == c); 
	if(c->next) assert(c->next->buckidx == c->buckidx); 
	if(c->prev) assert(c->prev->next == c); 
	if(c->prev) assert(c->prev->buckidx == c->buckidx); 
	c = c->next;
	nbcandidates++;    
      }
      // PRINTADV("}\n");      
    }    
    
    PRINTADV("[CHECK] nb candidates = %d in buckets %s\n", nbcandidates, bucketName[buckidx]);
    assert(s->nbcandidates[buckidx] == nbcandidates);    
  }
  
}

/* *********************************************************** */

static void kgggp_remove_vertex(struct kgggp_t * s, int thevtx)
{  
  for(int k = 0 ; k < s->nparts ; k++) {   
    struct candidate * c = s->list[k][thevtx];
    assert(c);    
    if(c->buckidx != KGGGP_BUCKET_NO) {
      assert(c->vtx == thevtx && c->part == k);
      PRINTADV("-> remove vertex %d (part %d) from buckets[%s][%d]\n", thevtx, k, bucketName[c->buckidx], s->maxgain-c->gain);    
      kgggp_bucket_remove(s, c);
    }
  }       
}

/* *********************************************************** */

static void kgggp_remove_part(struct kgggp_t * s, int thepart)
{  
  for(int i = 0 ; i < s->nvtxs ; i++) {   
    struct candidate * c = s->list[thepart][i]; 
    assert(c);
    if(c->buckidx != KGGGP_BUCKET_NO) {    
      assert(c->part == thepart && c->vtx == i);
      PRINTADV("-> remove vertex %d (part %d) from buckets[%s][%d]\n", i, thepart, bucketName[c->buckidx], s->maxgain-c->gain);    
      kgggp_bucket_remove(s, c);     
    }
  }       
}

/* *********************************************************** */

static void kgggp_move(struct kgggp_t * s, struct candidate * c, BucketIndexType oldbuckidx, int oldgain, BucketIndexType newbuckidx, int newgain)
{
  assert(c);  
  assert(oldbuckidx >= 0 && oldbuckidx < KGGGP_BUCKET_NBR);
  assert(newbuckidx >= 0 && newbuckidx < KGGGP_BUCKET_NBR);
  assert(c->buckidx == oldbuckidx);
  assert(c->gain == oldgain);
  kgggp_bucket_remove(s, c);
  c->gain = newgain; 
  c->buckidx = newbuckidx; 
  c->prev = c->next = NULL;
  kgggp_bucket_insert(s, c);
}

/* *********************************************************** */

/* select best candidate in buckets */
static struct candidate * kgggp_select(struct kgggp_t * s, 
				       Graph * g, 
				       int * currentwgt,         /* current weight array for all parts (of size nparts) */
				       int * targetwgt,          /* target weight array for all parts (of size nparts) */
				       int connectivity,         /* enforce connectivity */
				       int maxbestcandidates,
				       BucketIndexType buckidx,  /* bucket index */
				       int * nbmoves             /* nb inter-bucket moves (output) */
				       )
{
  // check
  assert(buckidx >= 0 && buckidx < KGGGP_BUCKET_NBR);
  
  /* ============ select candidates  ============ */
  
  /* HEURISTIC: take the better gain that checks the weight constraint
     (if it exists), else take the best gain even if weight constraint
     is not satisfied. */
  
  struct candidate * bestcandidate = NULL;
  
  /* look all candidates (not all, but nbbestcandidates <=
     maxbestcandidates) with best gain, to avoid side effects of vertices
     located in front of buckets... (use random select or second
     criteria) */
  
  struct candidate * bestcandidates[maxbestcandidates]; 
  for(int i = 0 ; i < maxbestcandidates ; i++) bestcandidates[i] = NULL;
  
  int nbbestcandidates = 0; 
  
  /* from best gain to lowest, look in buckets... */
  
  int v0 = s->maxgain - s->bestgain[buckidx]; // optimize access to bestgain in bucket
  // int v0 = 0;                              // no optimization
  
  for(int v = v0 ; v < s->bucketsize ; v++) { /* look in all buckets... */
    
    struct candidate * c = s->buckets[buckidx][v];
    
    PRINTADV("best candidates in buckets[%s][%d]: {", bucketName[buckidx], s->maxgain-v);
    
    /* iterate on linked-list */
    while(c) {
      assert(c->buckidx != KGGGP_BUCKET_NO);
      assert(s->maxgain - c->gain == v); 
      assert(c->buckidx == buckidx);     // select candidate only in buckets[buckidx]
      assert(c->gain <= s->bestgain[buckidx]);
      if(c->gain < s->bestgain[buckidx]) s->bestgain[buckidx] = c->gain; // update best gain (if lower)
      
      struct candidate * next = c->next; // save next candidate
      int w = g->vwgts?g->vwgts[c->vtx]:1;
      // int maxwgt = floor(targetwgt[c->part]*(100.0+ubfactor)/100.0); 
      
      /* if balance constraint is not respected in REG bucket */
      if ( (buckidx == KGGGP_BUCKET_REG) && ((currentwgt[c->part] + w) > targetwgt[c->part]) ) { 
	// printf("[DEBUG] vtx %d candidate for part %d move from REG to NBC bucket!!!\n", c->vtx, c->part);
	kgggp_move(s, c, KGGGP_BUCKET_REG, c->gain, KGGGP_BUCKET_NBC, c->gain);
	(*nbmoves)++;
      }
      /* if connectivity constraint is not respected in REG bucket */
      else if(connectivity && (buckidx == KGGGP_BUCKET_REG) && (currentwgt[c->part] > 0) && (c->conn == 0) ) {
	// printf("[DEBUG] vtx %d candidate for part %d move to NCC bucket!!!\n", c->vtx, c->part);
	kgggp_move(s, c, KGGGP_BUCKET_REG, c->gain, KGGGP_BUCKET_NCC, c->gain); // remove it !!!
	(*nbmoves)++;
      }
      /* if balance constraint is not respected in NCC bucket */
      else if (connectivity && (buckidx == KGGGP_BUCKET_NCC) && ((currentwgt[c->part] + w) > targetwgt[c->part]) ) { 
	// printf("[DEBUG] vtx %d candidate for part %d move from NCC to NBC bucket!!!\n", c->vtx, c->part);
	kgggp_move(s, c, KGGGP_BUCKET_NCC, c->gain, KGGGP_BUCKET_NBC, c->gain);
	(*nbmoves)++;	
      }
      else {
	bestcandidates[nbbestcandidates] = c; 
	nbbestcandidates++; 
	if(nbbestcandidates == maxbestcandidates) break;
      }  	
      
      PRINTADV("(gain=%d,vtx=%d,part=%d) ", c->gain, c->vtx, c->part);
      
      // next candidate in buckets[buckidx]      
      c = next; 
    }
    
    PRINTADV("}\n");
    
    // look only in first liked-list (best gain) if at least 1 candidate...
    if(nbbestcandidates >= 1) break;      
  }    
  
  // randomly select one of the best candidates found...
  if(nbbestcandidates == 1) {  
    bestcandidate = bestcandidates[0];
  }
  else if(nbbestcandidates > 0) {  
    int x = rand() % nbbestcandidates;
    assert(x >= 0 && x < nbbestcandidates);
    bestcandidate = bestcandidates[x];
  }
  
  if(bestcandidate) assert(bestcandidate->buckidx == buckidx);  
  return bestcandidate;
}

/* *********************************************************** */

static void kgggp_update(struct kgggp_t * s, 
		  Graph * g, 
		  int * part, 
		  int thevtx, 
		  int thepart,
		  int gainscheme,
		  int internaledgefactor,		  
		  int greedy,
		  int connectivity)
{  
  assert(part[thevtx] == thepart); // check vertex thevtx has already moved in thepart 
  
  // update the neighborhood of vertex thevtx
  for(int j = g->xadj[thevtx] ; j < g->xadj[thevtx+1] ; j++) {      
    int ii =  g->adjncy[j]; /* edge (thevtx,ii) */
    
    if(part[ii] != -1) continue; // skip, already assigned, no update required 
    
    for(int k = 0 ; k < s->nparts ; k++) {
      struct candidate * c = s->list[k][ii];
      assert(c);
      
      // GREEDY LOCAL: insert neighborhood candidates      
      if(greedy == KGGGP_GREEDY_LOCAL && c->buckidx == KGGGP_BUCKET_NO && k == thepart) {
	kgggp_candidate_init(g, part, c, ii, thepart, gainscheme, internaledgefactor);
	assert(c->gain >= -s->maxgain && c->gain <= s->maxgain);	
	kgggp_bucket_insert(s, c); // insert in KGGGP_BUCKET_REG 
      }
      
      // GREEDY GLOBAL/LOCAL: update neighborhood candidates      
      else if(c->buckidx != KGGGP_BUCKET_NO) { // candidate not removed
	
	assert(c->part == k);
	
	int oldgain = c->gain;   
	int newgain = c->gain;
	int oldbuckidx = c->buckidx;
	int newbuckidx = c->buckidx;	
	int updategain = kgggp_gain_update(g, part, thevtx, thepart, j, k, gainscheme, internaledgefactor); // compute gain to be updated
	
	// update connectivity
	if(thepart == c->part) {
	  c->conn++;
	  if(connectivity && c-> buckidx == KGGGP_BUCKET_NCC && c->conn > 0) {
	    PRINTADV("vtx %d candidate for part %d move back to REG bucket\n", c->vtx, c->part);
	    newbuckidx = KGGGP_BUCKET_REG;
	  }
	}
	
	// check update gain computation !
#ifdef DEBUGADV
	int _newgain = 0;
	for(int jj = g->xadj[ii] ; jj < g->xadj[ii+1] ; jj++)  
	  _newgain +=kgggp_gain_init(g, part, ii, k, jj, gainscheme, internaledgefactor); // vertex ii move to k (relative to edge jj)
	assert(newgain + updategain == _newgain);
#endif
	
	newgain += updategain;
	if(updategain == 0) continue; // no update required :-)            
	assert(newgain >= -s->maxgain && newgain <= s->maxgain);
	assert(ii == c->vtx);
	PRINTADV("-> update vertex %d if moving in part %d (old gain %d, new gain %d=%d)\n", c->vtx, k, oldgain, newgain, _newgain);      
	kgggp_move(s, c, oldbuckidx, oldgain, newbuckidx, newgain); // move updated candidate (as first item in list) and update gain...
	assert(c->gain == newgain);
	
      }
      
    }
  }  
  
}


/* *********************************************************** */

/* compute seeds to initiate KGGGP partitioning */  
static void kgggp_seeds(Graph * g, int nparts, int useseeds, int * part)
{
  if(useseeds == KGGGP_NO_SEEDS) return;
  
  // KGGGP_USE_SEEDS 
 
  int nbseeds = kgggp_computeGraphSeeds(g, nparts, 0, part);

#ifdef DEBUG
  int dist = kgggp_computeSeedDistance(g, nparts, part);
  PRINT("INFO: %d seeds computed (min dist = %d)\n", nbseeds, dist);
#endif
  
  /* #ifdef DEBUG_MESH         */
  /*   // mesh debug */
  /*   if(info && info->mesh && info->mesh->nb_cells == g->nvtxs) */
  /*     addMeshVariable(info->mesh, "kgggp-seeds", CELL_INTEGER, 1, part);       */
  /*   else if(info && info->mesh && info->mesh->nb_nodes == g->nvtxs) */
  /*     addMeshVariable(info->mesh, "kgggp-seeds", NODE_INTEGER, 1, part); */
  /* #endif */
  
  /* grow seeds as bubble  */
  if(useseeds == KGGGP_USE_BUBBLES) {
    int maxpartsize = g->nvtxs / (nparts*5); /* init bubble 20% */
    PRINT("INFO: initial bubble size = %d\n", maxpartsize);   
    kgggp_computeGraphBubbles(g, nparts, maxpartsize, part);
  }
  
  /* #ifdef DEBUG_MESH               */
  /*     // mesh debug */
  /*     if(info && info->mesh && info->mesh->nb_cells == g->nvtxs) */
  /*       addMeshVariable(info->mesh, "kgggp-bubbles", CELL_INTEGER, 1, part);       */
  /*     else if(info && info->mesh && info->mesh->nb_nodes == g->nvtxs) */
  /*       addMeshVariable(info->mesh, "kgggp-bubbles", NODE_INTEGER, 1, part); */
  /* #endif       */
  
}

/* *********************************************************** */

/* single pass */
static void kgggp_part_pass(Graph * g,       			 
		     int nparts,      
		     int ubfactor,    
		     int gainscheme,
		     int greedy,
		     int connectivity,
		     int internaledgefactor,
		     int maxbestcandidates,
		     int shrinking, 		     
		     int * part,      
		     int * partorder) 
{    
  /* compute egdecuts */
#ifdef DEBUGADV
  int edgecuts[nparts];
  int edgecut = kgggp_compute_edgecuts(g, nparts, part, edgecuts);  
  PRINTADV("initial edgecuts: [");   
  for(int k = 0 ; k < nparts ; k++) PRINTADV("%d ", edgecuts[k]);
  PRINTADV("]\n");
  PRINTADV("initial edgecut: %d\n", edgecut);   
#endif
  
  /* compute max gain (for free vertices) */
  int maxgain = kgggp_compute_maxgain(g, part);
  maxgain *= internaledgefactor;
  PRINTADV("max gain of free vertices: %d\n", maxgain);
  
  /* initialize buckets... */
  struct kgggp_t s;
  kgggp_alloc(&s, maxgain, nparts, g->nvtxs);
  kgggp_init(&s, g, part, gainscheme, internaledgefactor, greedy);  
  // kgggp_check(&s, g, part); // DEBUG  
  
  /* compute balance constraints */
  int currentwgt[nparts];
  int targetwgt[nparts];
  int totwgt = 0;
  int shrinksize = 0;
  for(int k = 0 ; k < nparts ; k++) currentwgt[k] = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) {
    if(part[i] == -1) shrinksize++;
    else currentwgt[part[i]] += g->vwgts?g->vwgts[i]:1;
    totwgt += g->vwgts?g->vwgts[i]:1;
  }
  
  int meanwgt = totwgt / nparts;
  int maxwgt = floor(meanwgt*(100.0+ubfactor)/100.0);   
  for(int k = 0 ; k < nparts ; k++) targetwgt[k] = maxwgt;
  
  if(shrinking == KGGGP_SHRINKING_YES)
    targetwgt[0] = 0; // first part is not growing as others (shrinking part)
  
  PRINT("INFO: KGGGP ubfactor = %d\n", ubfactor);      
  PRINT("INFO: KGGGP target mean wgt = %d\n", meanwgt);
  PRINT("INFO: KGGGP target max wgt = %d\n", maxwgt);  
  
  // initialise partorder with fixed vertices
  int _step = 0;
  if(partorder) {
    for(int i = 0; i < g->nvtxs ; i++) partorder[i] = -1;
    for(int i = 0; i < g->nvtxs ; i++) if(part[i] > -1) partorder[_step++] = i; 
  }
  
  /* ******** MAIN LOOP of KGGGP ALGORITHM ******** */
  
  int nbsteps = shrinksize;    
  int step = 0;
  int nbmoves = 0;
  int curwgt = 0;  
  int gains[nparts];
  for(int k = 0 ; k < nparts ; k++) gains[k] = 0;
  int totalgain = 0; 
  int moves[KGGGP_BUCKET_NBR]; 
  int weights[KGGGP_BUCKET_NBR];
  for(int i = 0 ; i < KGGGP_BUCKET_NBR ; i++) moves[i] = weights[i] = 0;  
  
  while(shrinksize > 0) {
    
#ifdef DEBUGADV
    kgggp_check(&s, g, part);
    
    PRINTADV("\n====== STEP %d (shrink size %d) ======\n", step, shrinksize);    
    PRINTADV("[KGGGP] step %d, nb candidates in buckets[REG]: %d\n", step, s.nbcandidates[KGGGP_BUCKET_REG]);
    if(connectivity) PRINTADV("[KGGGP] step %d, nb candidates in buckets[NCC]: %d\n", step, s.nbcandidates[KGGGP_BUCKET_NCC]);
    PRINTADV("[KGGGP] step %d, nb candidates in buckets[NBC]: %d\n", step, s.nbcandidates[KGGGP_BUCKET_NBC]);        
#endif
    
    /* select the best candidate  */    
    struct candidate * bestcandidate = NULL;
    /* select in REG bucket structure (weight constraint respected) */
    bestcandidate = kgggp_select(&s, g, currentwgt, targetwgt, connectivity, maxbestcandidates, KGGGP_BUCKET_REG, &nbmoves); 
    /* select in NCC bucket structure (no connectivity constraint respected) */
    if(connectivity && !bestcandidate) bestcandidate = kgggp_select(&s, g, currentwgt, targetwgt, connectivity, maxbestcandidates, KGGGP_BUCKET_NCC, &nbmoves); 
    /* select in NBC bucket structure (no weight constraint respected) */
    if(!bestcandidate) bestcandidate = kgggp_select(&s, g, currentwgt, targetwgt, connectivity, maxbestcandidates, KGGGP_BUCKET_NBC, &nbmoves); 
    /* error */
    if(!bestcandidate) {fprintf(stderr, "KGGGP failure: no more candidates in buckets!!!\n"); }    
    
    // warning
#ifdef WARNING
    if(bestcandidate && bestcandidate->buckidx == KGGGP_BUCKET_NBC)
      fprintf(stderr, "KGGGP WARNING: NBC bucket used at step %d !\n", step);
#endif
    
    // switching from local greedy approach to global...
    if(greedy == KGGGP_GREEDY_LOCAL && s.nbcandidates[KGGGP_BUCKET_REG] == 0 && bestcandidate->buckidx != KGGGP_BUCKET_REG) {
      PRINT("KGGGP INFO: switching from LOCAL to GLOBAL GREEDY approach at step %d (%.2f %% of steps completed)!\n", step, step*100.0/nbsteps);
      kgggp_reset(&s, nparts, g->nvtxs);
      kgggp_init(&s, g, part, gainscheme, internaledgefactor, KGGGP_GREEDY_GLOBAL);
      greedy = KGGGP_GREEDY_GLOBAL;
      continue; // continue main loop and try to select best candidates...            
    }
    
    if(bestcandidate) {
      PRINTADV("[KGGGP]step %d: vtx %d candidate for part %d is selected from %s bucket\n",
	       step, bestcandidate->vtx, bestcandidate->part, bucketName[bestcandidate->buckidx]);
    }
    
    assert(bestcandidate);
    
    // this is the best candidate...
    int thevtx = bestcandidate->vtx;
    int thepart = bestcandidate->part;
    int thegain = bestcandidate->gain;
    int thebuckidx = bestcandidate->buckidx;
    int thevwgt = g->vwgts?g->vwgts[thevtx]:1;       
    
    // statistic
    moves[thebuckidx]++;
    weights[thebuckidx] += thevwgt;
    
    PRINTADV("=> found best candidate: move vertex %d to part %d (gain %d, weight %d)\n", thevtx, thepart, thegain, currentwgt[thepart]);
    assert(thevtx >= 0 && thevtx < g->nvtxs);
    
    /* move it... */
    // assert(list[thepart][thevtx] == buckets[maxgain-thegain]);
    gains[thepart] -= thegain;
    totalgain -= thegain;
    assert(part[thevtx] == -1);
    part[thevtx] = thepart;
    currentwgt[thepart] += thevwgt;
    curwgt += thevwgt;
    shrinksize--;    
    
    if(partorder) {partorder[_step++] = thevtx; }
    
    /* clean buckets... */
    kgggp_remove_vertex(&s, thevtx);
    
    /* update gain for neighborhood of vertex thevtx moving to thepart... */
    kgggp_update(&s, g, part, thevtx, thepart, gainscheme, internaledgefactor, greedy, connectivity);
    
    
#ifdef DEBUGADV
    /* gains */
    PRINTADV("gains: [");   
    for(int k = 0 ; k < nparts ; k++) PRINTADV("%d ", gains[k]);
    PRINTADV("]\n");
    PRINTADV("total gain: %d\n", totalgain);       
    /* check edge cuts */
    int checkcuts[nparts];
    int checkcut = kgggp_compute_edgecuts(g, nparts, part, checkcuts);  
    PRINTADV("check edgecuts: [");   
    for(int k = 0 ; k < nparts ; k++) PRINTADV("%d ", checkcuts[k]);
    PRINTADV("]\n");
    PRINTADV("check edgecut: %d\n", checkcut);   
#endif    
    
    
    /* #ifdef DEBUG_MESH */
    /*     int period = MAX((nbsteps / DEBUG_PERIOD), 1);     */
    /*     /\* saving mesh partition for debug *\/ */
    /*     if(info && info->mesh && ((step % period == 0)  || (step == nbsteps-1))) { */
    /*       char varname[255]; */
    /*       sprintf(varname,"step-%06d", step); */
    /*       if(info->mesh->nb_cells == g->nvtxs) */
    /* 	addMeshVariable(info->mesh, varname, CELL_INTEGER, 1, part);       */
    /*       else if(info->mesh->nb_nodes == g->nvtxs) */
    /* 	addMeshVariable(info->mesh, varname, NODE_INTEGER, 1, part); */
    /*       else { assert(0); } /\* error *\/ */
    /*       PRINTADV("=> saving mesh partition: %s\n", varname); */
    /*     } */
    /* #endif */
    
    // periodic debug status 
#ifdef DEBUGADV
    int period = MAX((nbsteps / DEBUG_PERIOD), 1);
    if((step % period == 0)  || (step == nbsteps-1)) {
      PRINTADV("KGGGP@step%d: currentwgt [", step);
      for(int i = 0 ; i < nparts ; i++) PRINTADV("%d ", currentwgt[i]);
      PRINT("]\n");
    }
#endif
    
    // next step
    step++;      
    
    if(shrinking == KGGGP_SHRINKING_YES) {
      if(curwgt >= (totwgt-meanwgt) ) { PRINT("Skrinking mode: stop growing at step %d / %d\n", step, nbsteps); break; }
    }      
    
    
  } 
  /* END OF MAIN LOOP */  
  

  if(partorder) assert(_step == g->nvtxs);
  
  // DEBUG
  PRINT("INFO: moves for REG bucket structure = %d (weight = %.2f%%)\n", moves[KGGGP_BUCKET_REG], weights[KGGGP_BUCKET_REG]*100.0/totwgt);
  PRINT("INFO: moves for NCC bucket structure = %d (weight = %.2f%%)\n", moves[KGGGP_BUCKET_NCC], weights[KGGGP_BUCKET_NCC]*100.0/totwgt);
  PRINT("INFO: moves for NBC bucket structure = %d (weight = %.2f%%)\n", moves[KGGGP_BUCKET_NBC], weights[KGGGP_BUCKET_NBC]*100.0/totwgt);
  PRINT("INFO: total nb of inter-bucket moves = %d\n", nbmoves);
  
  // warning
#ifdef WARNING
  if(moves[KGGGP_BUCKET_NCC] > 0) fprintf(stderr, "KGGGP WARNING: NCC bucket used %d times (weight = %.2f%%)!\n", moves[KGGGP_BUCKET_NCC], weights[KGGGP_BUCKET_NCC]*100.0/totwgt);
  if(moves[KGGGP_BUCKET_NBC] > 0) fprintf(stderr, "KGGGP WARNING: NBC bucket used %d times (weight = %.2f%%)!\n", moves[KGGGP_BUCKET_NBC], weights[KGGGP_BUCKET_NBC]*100.0/totwgt);
#endif
  
  if(shrinking == KGGGP_SHRINKING_YES) {
    for(int i = 0 ; i < g->nvtxs ; i++) if(part[i] == -1) part[i] = 0;
  }
  
  // check
#ifdef DEBUG
  for(int i = 0 ; i < g->nvtxs ; i++) assert(part[i] != -1); 
#endif
  
  /* free memory */
  kgggp_free(&s);  
}

/* *********************************************************** */
/*                  PUBLIC ROUTINE                             */
/* *********************************************************** */

void kgggp_part(Graph * g,       			 
		int nparts,      
		int ubfactor,    
		int gainscheme,
		int greedy,
		int connectivity,
		int useseeds,
		int npass,
		int internaledgefactor,
		int maxbestcandidates,
		int shrinking,
		int * part,      
		int * partorder) 
{
  // check
  assert(g);
  assert(g->nvtxs >= nparts);
  assert(connectivity == KGGGP_CONNECTIVITY_NO || connectivity == KGGGP_CONNECTIVITY_YES);  
  assert(shrinking == KGGGP_SHRINKING_NO || shrinking == KGGGP_SHRINKING_YES);
  assert(gainscheme == KGGGP_GAIN_CLASSIC || gainscheme == KGGGP_GAIN_DIFF || gainscheme == KGGGP_GAIN_HYBRID);
  assert(greedy == KGGGP_GREEDY_GLOBAL || greedy == KGGGP_GREEDY_LOCAL);    
  if(greedy == KGGGP_GREEDY_LOCAL) assert(useseeds != KGGGP_NO_SEEDS);  
  for(int i = 0 ; i < g->nvtxs ; i++) assert(part[i] >= -1 && part[i] < nparts);
  
  // debug info
  PRINT("INFO: KGGGP nparts = %d\n", nparts);
  PRINT("INFO: KGGGP ubfactor = %d %%\n", ubfactor);
  PRINT("INFO: KGGGP gain scheme = %d\n", gainscheme);      
  PRINT("INFO: KGGGP enforce connectivity = %d\n", connectivity);
  PRINT("INFO: KGGGP greedy global (or local) = %d\n", greedy);
  PRINT("INFO: KGGGP use seeds = %d\n", useseeds);
  PRINT("INFO: KGGGP nb passes = %d\n", npass);        
  PRINT("INFO: KGGGP internal edge factor = %d\n", internaledgefactor);    
  PRINT("INFO: KGGGP max nb of best candidates = %d\n", maxbestcandidates);
  PRINT("INFO: KGGGP shrinking = %d\n", shrinking);      
  
  /* initialize seeds if asked */
  kgggp_seeds(g, nparts, useseeds, part);
  
  // single pass
  double _maxub = 0.0;
  int _edgecut = 0;
  if(npass == 1) { kgggp_part_pass(g, nparts, ubfactor, gainscheme, greedy, connectivity, internaledgefactor, maxbestcandidates, shrinking, part, partorder);  return; }
  
  // perform several passes
  int * bestpart = malloc(g->nvtxs*sizeof(int));
  int * _part = malloc(g->nvtxs*sizeof(int));
  
  int * _partorder = NULL;
  if(partorder) _partorder = malloc(g->nvtxs*sizeof(int));
  
  int bestedgecut = INT_MAX;
  
  for(int i = 0 ; i < npass ; i++) {
    memcpy(_part, part, g->nvtxs*sizeof(int)); 
    kgggp_part_pass(g, nparts, ubfactor, gainscheme, greedy, connectivity, internaledgefactor, maxbestcandidates, shrinking, _part, _partorder);
    _edgecut = kgggp_compute_edgecuts(g, nparts, part, NULL);
    _maxub = kgggp_compute_maxub(g, nparts, part);    
    PRINT("KGGGP: pass #%d -> edgecut = %d, maxub = %.6f\n", i, _edgecut, _maxub);
    // save part if better
    if( (_edgecut < bestedgecut && _maxub*100.0 <= (ubfactor+EPSILON)) || (bestedgecut == INT_MAX && i == npass-1) ) {
      bestedgecut = _edgecut;
      memcpy(bestpart, _part, g->nvtxs*sizeof(int));
      if(partorder) memcpy(_partorder, partorder, g->nvtxs*sizeof(int));
    }
  }
  memcpy(part, bestpart, g->nvtxs*sizeof(int));
  if(partorder) memcpy(partorder, _partorder, g->nvtxs*sizeof(int));
  PRINT("KGGGP: best edgecut = %d\n", bestedgecut);
  
  // free memory  
  if(partorder) free(_partorder);
  free(bestpart);
  free(_part);        
}

/* *********************************************************** */

