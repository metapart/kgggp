#ifndef __STRUCT_H__
#define __STRUCT_H__

//@{

/* *********************************************************** */
/*                          GRAPH                              */
/* *********************************************************** */

/** Graph CSR structure. */
typedef struct _Graph {  
  int nvtxs;         /**< nb vertices */
  int nedges;        /**< nb edges */
  int * xadj;        /**< array of indirection on adjncy (of size nvtxs+1) */
  int * adjncy;      /**< adjacency array of the CSR structure (of size 2*nedges) */
  int * vwgts;       /**< array vertex weights (of size nvtxs) */
  int * ewgts;       /**< array vertex edges (of size 2*nedges) */
} Graph;

//@}

#endif
