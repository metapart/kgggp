#ifndef __KGGGP_H__
#define __KGGGP_H__

typedef struct _Graph Graph; // forward declaration

//@{

/* *********************************************************** */
/*                  KGGGP PARAMETERS                           */
/* *********************************************************** */

enum { KGGGP_GAIN_CLASSIC, KGGGP_GAIN_HYBRID, KGGGP_GAIN_DIFF};
enum { KGGGP_NO_SEEDS, KGGGP_USE_SEEDS, KGGGP_USE_BUBBLES };
enum { KGGGP_CONNECTIVITY_NO, KGGGP_CONNECTIVITY_YES };
enum { KGGGP_GREEDY_LOCAL, KGGGP_GREEDY_GLOBAL};
enum { KGGGP_SHRINKING_NO = 0, KGGGP_SHRINKING_YES = 1};

#define KGGGP_INTERNALEDGEFACTOR 1
#define KGGGP_MAXBESTCANDIDATES 10

/* *********************************************************** */

void kgggp_part(Graph * g,       			 
		int nparts,      
		int ubfactor,    
		int gainscheme,
		int greedy,
		int connectivity,
		int useseeds,
		int npass,
		int internaledgefactor,
		int maxbestcandidates,
		int schrinking,
		int * part,      
		int * partorder);


/* *********************************************************** */
//@}


#endif
