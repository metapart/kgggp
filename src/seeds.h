#ifndef __SEEDS_H__
#define __SEEDS_H__

#include "struct.h"

//@{

/** Compute seeds (for all parts without fixed vertices already assigned) */
/** \return the nb seeds computed */
int kgggp_computeGraphSeeds(Graph * g,     /**< [in] graph */
		      int nparts,    /**< [in] nb of parts */
		      int optimize,  /**< [in] try to optimize seeds (0 or 1) */
		      int * part     /**< [in/out] array with input fixed vertices and output seeds (array of size g->nvtxs) */
		      );

/** return the minimum distance between all seeds */
int kgggp_computeSeedDistance(Graph * g,     /**< [in] graph */
			int nparts,    /**< [in] nb of parts */
			int * part     /**< [in] array with input seeds (array of size g->nvtxs, -1 if free vtx) */
			);

/** compute graph bubbles from input seeds */
void kgggp_computeGraphBubbles(Graph *g,         /**< [in]  input graph */
			 int size,         /**< [in]  nb parts */
			 int maxpartsize,  /**< [in]  max part size allowed (-1 if infinite) */
			 int * part        /**< [in/out]  input/output partition (with seeds) */
			 ); 


//@}

#endif
