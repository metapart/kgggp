#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "queue.h"

/* *********************************************************** */

void queue_init (Queue *self, int size) {
  self->size = size+1;
  self->tab = malloc (self->size * sizeof (int));
  self->start = 0;
  self->end = 0;
}

/* *********************************************************** */

void queue_push_back (Queue* self, int v) {
  self->tab[self->end++] = v;
  if (self->end >= self->size)
    self->end -= self->size;
}

/* *********************************************************** */

int queue_pop_front (Queue* self) {
  int v = self->tab[self->start++];
  if (self->start >= self->size)
    self->start -= self->size;
  return v;
}

/* *********************************************************** */

int queue_is_empty (const Queue *self) {
  return self->start == self->end;
}

/* *********************************************************** */

void queue_free (Queue *self) {
  free (self->tab);
}

/* *********************************************************** */
