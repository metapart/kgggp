#ifndef __DEBUG_H__
#define __DEBUG_H__

// debug
#if defined(DEBUG) || defined(DEBUGADV)
#define PRINT(...)                                                 \
  do {                                                             \
  fprintf(stderr, __VA_ARGS__);                                    \
  } while(0)
#else
#define PRINT(...) do {} while(0)
#endif
    
// advanced debug
#if defined(DEBUGADV)
#define PRINTADV(...)                                              \
  do {                                                             \
  fprintf(stderr, __VA_ARGS__);                                    \
  } while(0)
#else
#define PRINTADV(...) do {} while(0)
#endif

// info
#define PRINTINFO(...)                                             \
  do {                                                             \
  fprintf(stderr, __VA_ARGS__);                                    \
  } while(0)

#endif
